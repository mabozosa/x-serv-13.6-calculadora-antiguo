#!/usr/bin/python3

__author__ = 'MABB'

''' Calculadora simple con funcion (suma,resta,multiplicacion,division) '''


def Calculadora(funcion, operando1, operando2):

    funcion = str(funcion)
    operando1 = float(operando1)
    operando2 = float(operando2)

    if funcion == 'suma':
        print (operando1 + operando2)
    elif funcion == 'resta':
        print (operando1 - operando2)
    elif funcion == 'multiplicacion':
        print (operando1 * operando2)
    elif funcion == 'division':
        print (operando1 / operando2)
    else:
        print ('funciones: suma, resta, multiplicacion, division.')

try:
    print ('Sumar 1 y 2:')
    Calculadora('suma', 1, 2)
    print()
    print ('Sumar 3 y 4:')
    Calculadora('suma', 3, 4)
    print()
    print ('Restar 5 de 6:')
    Calculadora('resta', 6, 5)
    print()
    print ('Restar 7 de 8:')
    Calculadora('resta', 8, 7)

except ValueError:
    print("Introduzca un tipo correcto. \
          Los operandos deben ser valores numericos")

except ZeroDivisionError:
    print('El divisor no puede ser 0. Introduzca otro valor para el operando2')
